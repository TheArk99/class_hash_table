#include "hashtable.h"


//constructor
HashTable::HashTable(int s){
  size = s;
  List = new list * [size];
}


//destructor
HashTable::~HashTable(void){
  for (int i = 0; i < size; i++){
    list *temp = List[i];
    if (temp == nullptr)
      break;

    while(temp->next != nullptr){
      delete temp->previous;
      temp = temp->next;
    }
    delete temp;
  }

  delete List;
}


/*
void HashTable::changeSize(int s){

  PhoneBook **oldList = List;
  List = new PhoneBook * [s];
  memcpy(List, oldList, sizeof(PhoneBook*) * size);
  size = s;
  delete[] oldList;
}
*/

int HashTable::getSize(void){
  return size;
}


//hash func provided by dr hwang
unsigned long HashTable::djb2Hash(string key) {
  unsigned long hashVal = 5381;
  unsigned int i = 0;
  for (i = 0; i < key.length(); i++) {
    hashVal = ((hashVal << 5) + hashVal) + (key[i]); // hash *33 + c
  }
  return hashVal;
}

void HashTable::insert(string n, string p){
  PhoneBook pb(n, p);
  list *tempPtrToInsert = new list(pb);
  int index = djb2Hash(n) % size;

  if (index >= size || index < 0)
    return;

  if (List[index] == nullptr){
    List[index] = tempPtrToInsert;
  }else{
    list *tempPtr = List[index];
    while(tempPtr->next != nullptr){
      if (tempPtr->data.name == tempPtrToInsert->data.name || tempPtr->next->data.name == tempPtrToInsert->data.name){
        //cout << "Can not have same name / key as one already in use!" << endl;
        return;
      }
      tempPtr = tempPtr->next;
    }
    if (tempPtr->data.name == tempPtrToInsert->data.name){
      //cout << "Can not have same name / key as one already in use!" << endl;
      return;
    }
    tempPtr->next = tempPtrToInsert;
    tempPtr->next->previous = tempPtr;
  }
}

void HashTable::printTable(void){
  if (List == nullptr)
    return;

  for (int i = 0; i < size; i++){
    list *temp = List[i];
    if (temp == nullptr){
      continue;
    }

    while(temp->next != nullptr){
      cout << temp->data.name << endl;
      cout << temp->data.phoneNum << endl;
      cout << endl;
      temp = temp->next;
    }
  }

}

HashTable::list *HashTable::search(string n){
  int index = djb2Hash(n) % size;

  if (index >= size || List[index] == nullptr){
    return nullptr;
  }else if (List[index] == nullptr){
    return List[index];
  }

  list *tempPtr = List[index];
  while(tempPtr->data.name != n && tempPtr->next != nullptr){
    tempPtr = tempPtr->next;
  }
  if (tempPtr->data.name == n){
    return tempPtr;
  }else{
    return nullptr;
  }
}


void HashTable::printSearch(string n){
  list *temp = search(n);
  if (temp == nullptr){
    cout << "No value found" << endl;
    cout << endl;
  }else{
    cout << "search found value!" << endl;
    cout << "name: " << temp->data.name << endl;
    cout << "phone number: " << temp->data.phoneNum << endl;
    cout << endl;
  }
}


void HashTable::remove(string n){
  int index = djb2Hash(n) % size;
  list *temp = search(n);
  if (temp == nullptr){
    cout << "No value found to remove!" << endl;
  }else{
    cout << "Removing..." << endl;
    if (temp->next != nullptr){
      temp->next->previous = temp->previous;
      temp->next = temp->next->next;
    }

    if (temp->previous != nullptr){
      temp->previous->next = temp->next;
    }

    if (temp == List[index]){
      List[index] = temp->next;
    }

    delete temp;
  }
}

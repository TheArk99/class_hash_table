#ifndef __HASHTABLE__
#define __HASHTABLE__

#include <iostream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <vector>
#include <cstring>
using namespace std;

class HashTable {
  private:
    struct PhoneBook{
      string name; // this will be the key
      string phoneNum;
      PhoneBook(string n = "", string p = "") : name(n), phoneNum(p) {};
    };

    struct list{
      PhoneBook data;
      list *next;
      list *previous;
      list(PhoneBook d, list *nPtr = nullptr, list *pPtr = nullptr) :
        data(d), next(nPtr), previous(pPtr) {};
    };

    int size;
    unsigned long djb2Hash(string key); // this will give hash from key (name) it will need to have mod after returned as it will not by default be in the same size as arr
    //vector<PhoneBook*> List; //recieveing corde dumps when trying to use vectors not sure why, tried objdumping and gdb but honestly i just do not know enough x86 asm
    //PhoneBook **List;
    list **List;

    list *search(string n);

  public:
    HashTable(int s = 50);
    ~HashTable(void);
    void printTable(void);
    void insert(string n, string p);
    void printSearch(string n);
    void remove(string n);
    //void changeSize(int s);//cant do with new, could do if using vector or realloc
    int getSize(void);
};


#endif

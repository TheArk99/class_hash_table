#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <stdio.h>
#include <ctime>
#include <fstream>
#include "hashtable.h"
using namespace std;

int main(int argc, char** argv){
  HashTable ht;

  ifstream File("hashdata.txt");

  if (!File.is_open()){
    cerr << "Error opening file!" << endl;
    return 1;
  }
  
  string line;
  while (getline(File, line)) {
    string name;
    string phone;

    size_t pos1 = line.find(';');
    
    if (pos1 != string::npos) {
      name = line.substr(0, pos1);
      size_t pos2 = line.find(';', pos1 + 1);
      if (pos2 != string::npos) {
        phone = line.substr(pos1 + 1, pos2 - pos1 - 1);
        ht.insert(name, phone);
      }
    }
  }
File.close();


  if (argc >= 3){
    for (int i = 1; i < argc - 1; i++){
      ht.insert(argv[i], argv[i+1]);

    }
  }


//  ht.insert("noah", "909-312-1431");



  bool continueToLoop = true;
  while (continueToLoop){
    cout << "1.) insert" << endl;
    cout << "2.) search" << endl;
    cout << "3.) remove" << endl;
    cout << "4.) print table" << endl;
//    cout << "5.) change size of bucket list" << endl;
 //   cout << "6.) exit" << endl;
    cout << "5.) exit" << endl;
    cout << ">>> ";

    string userInput;
    string name;
    string phone;
 //   string newSize;
    getline(cin, userInput);


    switch(stoi(userInput)){
      case 1:
        cout << "Name: ";
        getline(cin, name);
        cout << "Phone number: ";
        getline(cin, phone);
        ht.insert(name, phone);
        break;
      case 2:
        cout << "Name: ";
        getline(cin, name);
        ht.printSearch(name);
        break;
      case 3:
        cout << "Name: ";
        getline(cin, name);
        ht.remove(name);
        break;
      case 4:
        ht.printTable();
        break;
      /*
      case 5:
        cout << "What size would you like to increase it to?" << endl;
        cout << "Current size: " << ht.getSize() << endl;
        cout << ">>> ";
        getline(cin, newSize);
        if (stoi(newSize) <= ht.getSize()){
          cout << "Must enter size larger than current size!" << endl;
          break;
        }
        ht.changeSize(stoi(newSize));
        break;
      */
      //case 6:
      case 5:
        continueToLoop = false;
        break;
    }

  //  if (stoi(userInput) > 6){
    if (stoi(userInput) > 5){
      cout << "Enter correct input in the field!" << endl;
    }

    cout << endl << "Press Enter to Continue";
    getline(cin, userInput);
    system("clear");

  }

}

CXX := g++
CXXFlags := -Wall -g -pipe -O2 -march=native -D_GLIBCXX_ASSERTIONS -std=c++20

.PHONY: all clean obj

all: main

obj: src/hashtable.cpp
	$(CXX) -c src/hashtable.cpp -o objfiles/hashtable.o $(CXXFlags)

main: obj src/main.cpp
	$(CXX) -o bin/"$@" src/main.cpp objfiles/hashtable.o $(CXXFlags)

install: clean
	mkdir -p bin/
	mkdir -p objfiles/
	$(MAKE) all


clean:
	rm -rf bin/
	rm -rf objfiles/
